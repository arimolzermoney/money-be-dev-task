<div class="flex content-center	">

    <div class="p-8 w-full">
        <div
            class="block py-8 px-12 my-8 mx-0 min-w-full leading-5 text-white align-top bg-emerald-600 bg-no-repeat bg-none rounded"
            style="animation: 5s linear 0s 1 normal both running slide; text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;"
        >

            <div
                class="block p-0 mx-0 mt-0 mb-8 w-full leading-5 align-top text-zinc-800"
                style="text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;"
            >
                <div
                    class="px-0 pt-1 pb-px m-0 leading-5 text-zinc-800">
                    <label
                        class="block p-0 mx-0 mt-0 mb-1 text-base leading-5 cursor-default text-zinc-800"
                        style="font-weight: bold;">
                        I want to borrow ($):
                    </label>
                    <input
                        id="vehicle_purchase_price"
                        name="vehicle_purchase_price"
                        class="block py-2 px-2 pr-4 w-5/6 text-lg rounded cursor-text resize-none focus:border-gray-50 bg-white border border-solid border-neutral-200"
                        type="text"
                        min="0"
                        maxlength="10"
                        placeholder="0"
                        value=""
                        style="line-height: normal;"
                    />
                </div>

            </div>

            <div
                class="block p-0 mx-0 mt-0 mb-8 w-full leading-5 align-top text-zinc-800"
                style="text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;"
            >
                <div
                    class="px-0 pt-1 pb-px m-0 leading-5 text-zinc-800">
                    <label
                        class="block p-0 mx-0 mt-0 mb-1 text-base leading-5 cursor-default text-zinc-800"
                        style="font-weight: bold;">
                        At an interest rate of (%):
                    </label>
                    <input
                        id="vehicle_purchase_price"
                        name="vehicle_purchase_price"
                        class="block py-2 px-2 pr-4 w-5/6 text-lg rounded cursor-text resize-none focus:border-gray-50 bg-white border border-solid border-neutral-200"
                        type="text"
                        min="0"
                        maxlength="10"
                        placeholder="0"
                        value=""
                        style="line-height: normal;"
                    />
                </div>

            </div>

            <div
                class="block p-0 mx-0 mt-0 mb-8 w-full leading-5 align-top text-zinc-800"
                style="text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;"
            >
                <div
                    class="px-0 pt-1 pb-px m-0 leading-5 text-zinc-800">
                    <label
                        class="block p-0 mx-0 mt-0 mb-1 text-base leading-5 cursor-default text-zinc-800"
                        style="font-weight: bold;">
                        Over x Years:
                    </label>
                    <select class="block py-2 px-2 pr-4 w-5/6 text-lg rounded cursor-text resize-none focus:border-gray-50 bg-white border border-solid border-neutral-200">
                        <option value="1">1 Year</option>
                        <option value="3">3 Years</option>
                        <option value="5">5 Years</option>
                    </select>
                </div>
            </div>

            <div
                class="block py-0 pr-0 pl-2 m-0 w-2/3 text-left text-white align-top"
                style="line-height: 1.3; text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;"
            >

                <label
                    class="block p-0 mx-0 mt-0 mb-1 text-base text-left text-white opacity-50 cursor-default"
                    style="font-weight: bold; line-height: 135%; text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;"
                >
                    Your estimated repayments are
                </label>


                <h4
                    class="p-0 mx-0 mt-2 mb-1 text-6xl tracking-normal text-white"
                    style="font-weight: bold; line-height: 100%; text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;"
                >
                    ${{ number_format($result, 2) }}
                </h4>

                <div
                    class="table clear-both p-0 mx-0 mt-3 mb-0 leading-5 text-left text-white"
                    style='content: ""; text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;'
                >
                    <button
                        class="float-left overflow-visible py-2 px-6 my-1 mx-0 text-xs leading-4 text-center text-white normal-case border border-emerald-600 border-solid cursor-pointer bg-slate-400 hover:bg-slate-400 hover:text-white"
                        style="border-radius: 5px 0px 0px 5px;"
                    >
                        Monthly
                    </button>

                    <button
                        class="float-left overflow-visible py-2 px-6 my-1 mx-0 text-xs leading-4 text-center normal-case bg-teal-600 border border-emerald-600 border-solid cursor-pointer text-slate-400 hover:bg-slate-400 hover:text-white"
                    >
                        Fortnightly
                    </button>

                    <button
                        class="float-left overflow-visible py-2 px-6 my-1 mx-0 text-xs leading-4 text-center normal-case bg-teal-600 border border-emerald-600 border-solid cursor-pointer text-slate-400 hover:bg-slate-400 hover:text-white"
                        style="border-radius: 0px 5px 5px 0px;"
                    >
                        Weekly
                    </button>

                </div>
            </div>
        </div>
    </div>
</div>
