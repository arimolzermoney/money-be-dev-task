<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ExampleCalculator extends Component
{
    public float $result = 0.00;

    public float $loanAmount;

    public float $interestRate;

    public int $termDuration;

    public string $frequency = 'Month';

    protected array $rules = [
        'result' => '',
        'loanAmount' => '',
        'interestRate' => '',
        'termDuration' => '',
        'frequency' => ''
    ];

    public function mount()
    {

    }

    public function calculateRate()
    {
        // Calculate the estimated repayment for a loan, based on the loanAmount, interestRate, termDuration,
        // and display the amount in the selected frequency.
    }

    public function render()
    {
        return view('livewire.example-calculator');
    }
}
